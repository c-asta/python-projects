#!/usr/bin/env python3

"""
A short guessing game!
You have three chances to guess a number 1 through 10.
By: chris@asta.dev
"""

import random
import time
import tkinter

class Guessing_Game:

  def __init__(self, master):
    #Main window generation.
    self.master = master
    master.title("Guessing Game")
    master.geometry("450x125")

    #Inital guess count and random number generation.
    self.num_guesses = 3
    self.num = random.randint(1,10)

    #Title screen labels and start button generation.
    self.label1 = tkinter.Label(master, text="Welcome to the guessing game!")
    self.label1.pack()

    self.label2 = tkinter.Label(master, text="You have three chances to guess a number 1 through 10.")
    self.label2.pack()

    self.label3 = tkinter.Label(master, text="By: Christopher Asta (chris@asta.dev)")
    self.label3.pack()

    self.label4 = tkinter.Label(master, text="")
    self.label4.pack()

    self.start_button = tkinter.Button(master, text="Start", command=self.start_game)
    self.start_button.pack()

  #Invaild input method.
  def invalid_input(self):
    self.label4.configure(text="Invalid input. Try again.")
    self.guess_box.delete(0, tkinter.END)

  #Method to refesh the guess count after a guess.
  def refresh_guess_count(self):
    self.label2.configure(text=(("Guesses left: %d") % (self.num_guesses)))
    self.label4.configure(text="")

  #Method for "New Game" button. Repurposes the "Guess" button.
  def new_game_button(self):
    self.guess_button.configure(text="New Game", command=self.new_game)

  #Method to reset guesses, generate a new number and reconfigure labels for a new game.
  def new_game(self):
    self.num_guesses = 3
    self.num = random.randint(1,10)
    self.refresh_guess_count()
    self.guess_box.delete(0, tkinter.END)
    self.guess_button.configure(text="Guess", command=self.guessed)

  #Method to start the when the "Start" button is pressed.
  def start_game(self):
    #tkinter.Label reconfiguring for game start.
    self.label1.configure(text="Guess a number 1 through 10.")
    self.label3.pack_forget()
    self.label4.pack_forget()
    self.start_button.pack_forget()

    self.guess_box = tkinter.Entry(self.master, width=10)
    self.guess_box.pack()

    self.label4 = tkinter.Label(text="")
    self.label4.pack()
    
    self.refresh_guess_count()

    self.guess_button = tkinter.Button(self.master, text="Guess", command=self.guessed)
    self.guess_button.pack()

  #Method for the when the "Guess" button is pressed. All the game logic. 
  def guessed(self):
    #Main try loop for error catching.
    try:
      guess = int(self.guess_box.get())
      
      #If/else statment to catch guesses below 1 or above 10.
      if (guess < 1 or guess > 10):
        self.invalid_input()

      else:
        #"Thinking" animation.
        for x in range(0, 12):
          if x % 4 == 0:
            self.label4.configure(text="\\")
            self.master.update()
            time.sleep(.2)
          elif x % 4 == 2:
            self.label4.configure(text="/")
            self.master.update()
            time.sleep(.2)
          else:
            self.label4.configure(text="|")
            self.master.update()
            time.sleep(.2)

        #Win condition.
        if guess == self.num:
          self.label4.configure(text="That\"s right! You win!")
          self.new_game_button()

        #If the player didn"t win. Increments the guess count down.
        else:
          self.num_guesses -= 1
          self.refresh_guess_count()
          self.label4.configure(text="Nope. Guess again.")
          
          #Lose condition.
          if self.num_guesses < 1:
            self.label4.configure(text="Sorry you lose. The number was %s." % (self.num))
            self.new_game_button()

      #Clears guess box for new guess.
      self.guess_box.delete(0, tkinter.END)

    #Error catching for invalid inputs such as strings.
    except TypeError:
      self.invalid_input()
    except ValueError:
      self.invalid_input()


root = tkinter.Tk()
the_game = Guessing_Game(root)
root.mainloop()
