#!/usr/bin/env python3

"""
datetime_but_rainbow.py
Displays the datetime, but with rainbow colors.
By: chris@asta.dev
"""

"""
TODO:
Replace figlet and lolcat.
Dont run on a loop, but print the datetime
Add help
"""

###################################################################################################


import datetime
import libasta
import os
import random
import subprocess
import sys
import time

###################################################################################################

def main():

    terminal_columns, terminal_rows = os.get_terminal_size()
    font = "{}/hash_font.flf".format(os.path.dirname(sys.argv[0]))
    print(font)
    seed = random.randint(1,100) 
    date_format = "%Y-%m-%d %H:%M:%S"

    try:
        while True:
            libasta.clear_terminal()
            current_datetime = datetime.datetime.now().strftime(date_format) 
            sp_command = "figlet %s -w %s -f %s | lolcat -S %s" % (current_datetime, terminal_columns, font, seed)
            subprocess.call(sp_command, shell=True)
            time.sleep(.95)

    except KeyboardInterrupt:
        libasta.clear_terminal()
        pass

###################################################################################################

main()
