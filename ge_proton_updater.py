#!/usr/bin/env python3


import hashlib
import json
import os
import shutil
import sys
import requests
import tarfile


BLOCK_SIZE = 65536
INSTALL_LOCATION = os.path.expanduser("~/.steam/root/compatibilitytools.d/")
LATEST_RELEASE_URL = "https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases/latest"


def main():
    if not os.path.exists(os.path.expanduser("~/.steam")):
        print("~/.steam doesnt exist. Is steam installed?", file=sys.stderr)
        exit(1)

    # Get the latest release info
    try:
        response = requests.get(LATEST_RELEASE_URL)
        latest_release = json.loads(response.text)
        # Find download urls
        download_urls = []
        for i in range(len(latest_release["assets"])):
            download_urls.append(latest_release["assets"][(i-1)]["browser_download_url"])
        tag_name = latest_release["tag_name"]
        print("Latest GE Proton Tag: {0}".format(tag_name))
    except:
        print("Error getting latest release info from {0}".format(LATEST_RELEASE_URL), file=sys.stderr)
        exit(1)

    # Rather than delete the folder first. Wait till the download and hash check succeeds.    
    reinstall = check_install_location(tag_name)

    tmp_file = "/tmp/{0}{1}".format(tag_name,".tar.gz")
    # Go through the urls and download the sha512 sum and the .tar.gz
    for url in download_urls:
        if url.endswith(".sha512sum"):
            sha512sum = requests.get(url)

        if url.endswith(".tar.gz"):
            r = requests.get(url)
            open(tmp_file, "wb").write(r.content)

    hashes_match, file_hash = compare_hash(sha512sum.text, tag_name, tmp_file)
    if not hashes_match:
        print("File hash doesn't match!", file=sys.stderr)
        print("", file=sys.stderr)
        print("Provided sha512 sum: {0}".format(sha512sum.text), file=sys.stderr)
        print("Downloaded file sum: {0}".format(file_hash), file=sys.stderr)
        os.remove(tmp_file)
        exit(1)

    if reinstall == True:
        shutil.rmtree("{0}/{1}".format(INSTALL_LOCATION, tag_name))

    try:
        extract_files(tmp_file)
        os.remove(tmp_file)
    except:
        print("Extraction failed.", file=sys.stderr)
        os.remove(tmp_file)
        exit(1) 

    print("Installation succeeded! Please restart steam for it to be available.")


def check_install_location(tag_name):
    reinstall = False
    if not os.path.exists(INSTALL_LOCATION):
        os.mkdir(INSTALL_LOCATION)
    elif os.path.exists("{0}/{1}".format(INSTALL_LOCATION, tag_name)):
        print("{0} already exists in {1}".format(tag_name, INSTALL_LOCATION))
        response = input("Reinstall(Y/N)? ")
        if response not in ["Y", "y", "YES", "yes"]:
            exit(0)
        else:
            reinstall = True

    return reinstall


def compare_hash(sha512sum, tag_name, tmp_file):
    hashes_match = False
    file_hash = hashlib.sha512()
    with open(tmp_file, "rb") as f:
        file_block = f.read(BLOCK_SIZE)
        while len(file_block) > 0:
            file_hash.update(file_block)
            file_block = f.read(BLOCK_SIZE)

    formatted_file_hash = "{0}  {1}.tar.gz\n".format(file_hash.hexdigest(),tag_name)
    if sha512sum == formatted_file_hash:
        hashes_match = True

    return hashes_match, formatted_file_hash


def extract_files(tmp_file):
    tr = tarfile.open(tmp_file)
    tr.extractall(INSTALL_LOCATION)
    tr.close

    
main()
